# Blood Cells

### Подготовка проекта
Для работы нужен **python 3.5**:
```
sudo apt install python3.5 python3-pip
```
#### Создание вирутального окружения
```
pip3 install virtualenv
```
Далее, для создания виртуального окружения в папке с проектом:
```
virtualenv venv -p python3.5
source venv/bin/activate
```

#### Python зависимости
В папке с проектом: без поддержки вычислений на GPU
```
pip install -r requirements.txt
```
С поддержкой вычислений на GPU
```
pip install -r requirements_gpu.txt
```

#### CUDA ***(только для GPU)***
Для работы с GPU необходима установка [cuda](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/#axzz4VZnqTJ2A) (8.0) и [cudnn](https://developer.nvidia.com/cudnn) (6.0)


#### Вход в виртуальное окружение
Для **virtualenv** в папке с проектом выполнить:
```
source venv/bin/activate
```
#### Описание
Для разделения данных на категории используется ноутук Separate_images.ipynb.

Для аугментации данных используется ноутбук Data_augmentation.ipynb, в нем создаются и заполняются дирректории для записи train, test и validation данных.


Ноутбук Blood_Cells.ipynb создает модель нейронной сети и обучает на ней сеть для классификации типов клеток крови, используя аугментированные данные.
